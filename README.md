# GIMS

GIMS (Graphical Interface for Materials Simulations) is a toolbox for electronic structure codes and supports the generation of input files and the analysis of output files. There are the following elemental apps:
* **Structure Builder:** Visualize, create or modify atomic structures.
* **Control Generator:** Set up the numerical Settings for the calculations.
* **Output Analyzer:** Analyze and visualize the output files.

These elemental apps are connected in two workflows:
* **Simple Calculation:** Prepare everything for a single-point calculation and later analyze the results.
* **Band Structure:** Based on your (periodic) structure the  k-point path is automatically generated and the input files are set up accordingly.

Currently, GIMS supports the following electronic structure codes:
* [FHI-aims](https://aimsclub.fhi-berlin.mpg.de)
* [Exciting](http://exciting-code.org)

## Documentation

For a complete documentation of the whole project please visit: https://gims-developers.gitlab.io/gims/index.html

## Requirements

Python:

| Package | Version |
|---------|---------|
| ase     | 3.20.1  |
| flask   | 1.1.2   |
| spglib  | 1.15.0  |

Javascript:

| Package             | Version  |
|---------------------|----------|
| chart.js            | 3.5.0    |
| parcel              | 2.0.0    |
| regenerator-runtime | 0.13.8   |
| three               | 0.130.1  |

## Quick start commands

To build GIMS two steps are needed: 
1. Build the client (javascript) in the `gims/client`
2. Install the python package in the `gims/app` folder.

After that the development server from flask can be started and you can run the app locally on your machine.

### How to build the client

First, we get into the application `client/` folder:

```
cd gims/client/
```
Please install all dependencies with:
```
npm install --production=false
```
We use the **watch** command from the parcel bunlder, so we watch the source code and rebuild when necessary. This is activated with:

```
npm run dev
```
If you just want to build the client once, please use:
```
npm run build
```

The result of this build process is copied to the `app/gims/static/` folder.

\* WARNING: Depending on the execution environment the *parcel* command parameter `--public-url` and the `Conf.BASE_FOLDER` variable (in the `Conf.js` code file) could need to be changed.


### How to run the application (development)

We use the [Flask](http://flask.pocoo.org/) Python application server. The project [User's guide](http://flask.pocoo.org/docs/1.0/) explains quite well how to set up the server and run a application. In short, if you have build the client go to:
```
cd gims/app/
```
Install the package with:
```
pip install .[dev]
```
and type:
```
export FLASK_APP=gims
flask run
```
Now, open the browser and enter the URL:
```
localhost:5000
```
You should now see the top-level GIMS interface.

### Using Docker

A Dockerfile is provided to build an image that can run a docker container
with both the server and client.
Run the following from the top-level of the repository:

```
docker build -t gims .
docker run -p 5000:5000 gims
```
