/**
 * @author Andrey Sobolev
 *
 * @fileOverview tests for SupercellField module
 */

import SupercellField from "../../structure-builder-mod/SupercellField";
let field = new SupercellField()

describe('Supercell Field', () => {
  beforeEach(() => {
    document.body.appendChild(field.e)
  })

  test('matrix is shown', () => {
    let isMatrix = document.querySelector('.isMatrix')
    let matrixFields = document.querySelectorAll('input.matrix')
    isMatrix.checked = true
    isMatrix.dispatchEvent(new Event('change'))
    matrixFields.forEach( f => expect(f).toBeVisible() )
    isMatrix.checked = false
    isMatrix.dispatchEvent(new Event('change'))
    matrixFields.forEach( f => expect(f).not.toBeVisible() )
  });

  test('values are properly received', () => {
    let matrixFields = document.querySelectorAll('input.sc-value')
    matrixFields[0].value = '2'
    matrixFields[1].value = '2'
    matrixFields[2].value = '2'
    expect(field.getValues()).toEqual([2, 2, 2])
    matrixFields[1].value = ''
    expect(field.getValues()).toEqual([2, 0, 2])
  })

  test('values are properly set', () => {
    let matrixFields = document.querySelectorAll('input.sc-value')
    field.setValues(2)
    expect(field.getValues()).toEqual([2, 2, 2])
  })
})
