import OutputExciting from "../../../output-analyzer-mod/OutputExciting.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/Exciting/grst/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'
const FILE_NAME = 'INFO.OUT'

let fileContent = fs.readFileSync(FILES_FOLDER+FILE_NAME, "utf8")
const scfLoopsTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+FILE_NAME+TEST_OUTPUT_FILE_ENDING, "utf8"))

test('Output file: '+FILE_NAME+' - Object from parsing: scfLoops', () => {

  let outputInstance = new OutputExciting()
  outputInstance.parseFile(FILE_NAME, fileContent)

  // test output file generation -> fs.writeFile(FILES_FOLDER+FILE_NAME+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.scfLoops),() => {})

  const scfLoopsTestInput = outputInstance.scfLoops

  for (var i = 0; i < scfLoopsTestInput.length; i++) {
    const loopData = scfLoopsTestInput[i]
  for (const atom of loopData.structure.atoms) delete atom.radius
  expect(loopData.finalScfEnergies).toEqual(scfLoopsTestOutput[i].finalScfEnergies)
	expect(loopData.structure).toEqual(scfLoopsTestOutput[i].structure)
	expect(loopData.iterations).toEqual(scfLoopsTestOutput[i].iterations)
	expect(loopData.isConverged).toEqual(scfLoopsTestOutput[i].isConverged)
  }

})
