/**
 * @author Andrey Sobolev
 *
 * @fileOverview tests for Structure module
 */

import {getStructureFromJSON} from "../../common/util";
import Structure from "../../common/Structure";
import jsonStructure from './testcases/CdTaIn.json'

describe('Structure object', () => {

  let structure = new Structure()
  beforeEach(() => {
    structure = getStructureFromJSON(jsonStructure)
  })

  test('should reset', () => {
    structure.reset()
    expect(structure.atoms).toEqual([])
    expect(structure.latticeVectors).toBe(undefined)
  })

  test('should be periodic', () => {
    expect(structure.isAPeriodicSystem()).toBe(true)
    structure.removeLatticeVectors()
    expect(structure.isAPeriodicSystem()).toBe(false)
  })

  test('should be periodic', () => {
    expect(structure.isAPeriodicSystem()).toBe(true)
    structure.removeLatticeVectors()
    expect(structure.isAPeriodicSystem()).toBe(false)
  })


})
