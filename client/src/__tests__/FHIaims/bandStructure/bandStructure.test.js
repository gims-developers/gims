import OutputAims from "../../../output-analyzer-mod/OutputAims.js"
import * as fs from 'fs'

const FILES_FOLDER = 'src/__tests__/FHIaims/bandStructure/periodic/'
const TEST_OUTPUT_FILE_ENDING = '.test_output.json'

const FILE_NAMES = [
  'aims.out_bands',
  'control.in',
  'geometry.in',
  'KS_DOS_total.dat',
]
for (let i = 1; i <= 9; i++) {
  FILE_NAMES.push('band100'+i+'.out')
}

const fileObjectArray = []  // Prepare the array of files for parseFiles()
FILE_NAMES.forEach( fileName => {
  let content = fs.readFileSync(FILES_FOLDER+fileName, "utf8")
  fileObjectArray.push( {name: fileName, content: content} )
})

const scfLoopsTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+'aims.out_bands'+TEST_OUTPUT_FILE_ENDING, "utf8"))
const dosTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+'KS_DOS_total.dat'+TEST_OUTPUT_FILE_ENDING, "utf8"))
const bsInfoTestOutput = JSON.parse(fs.readFileSync(FILES_FOLDER+'bandStructureInfo'+TEST_OUTPUT_FILE_ENDING, "utf8"))


let outputInstance = new OutputAims()

test('Parse files', () => {
	outputInstance.parseFiles(fileObjectArray)
	expect(getParsedFiles(outputInstance)).toEqual(FILE_NAMES)
})

test('Parse output file', () => {
  outputInstance.parseOutputFile()
  // test output file generation -> fs.writeFile(FILES_FOLDER+'aims.out_bands'+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.scfLoops),() => {})
  testScfLoopData(outputInstance, scfLoopsTestOutput)
})

test('DOS data', () => {
  let dosData = outputInstance.getFirstFileDosData()
  //test output file generation -> fs.writeFile(FILES_FOLDER+'KS_DOS_total.dat'+TEST_OUTPUT_FILE_ENDING, JSON.stringify(dosData),() => {})
  expect(dosData).toEqual(dosTestOutput)
})

test('Band structure data', () => {
  let bsInfo = outputInstance.getBsInfo()
  // test output file generation -> fs.writeFile(FILES_FOLDER+'bandStructureInfo'+TEST_OUTPUT_FILE_ENDING, JSON.stringify(outputInstance.getBsInfo()),() => {})
  expect(bsInfo).toEqual(bsInfoTestOutput)
})



function getParsedFiles(output){

  const files = []
  output.files.output.forEach( (c, fileName) => files.push(fileName) )
  output.files.input.forEach( (c, fileName) => files.push(fileName) )
  output.files.dos.forEach( (c, fileName) => files.push(fileName) )
  output.segmentsMap.forEach( (c, bandNum) => files.push('band1'+bandNum+'.out') )
  return files
}


function testScfLoopData(output, scfLoopsTestOutput){

  const scfLoopsTestInput = JSON.parse(JSON.stringify(output.scfLoops))

  for (var i = 0; i < scfLoopsTestInput.length; i++) {
    const loopData = scfLoopsTestInput[i]//outputInstance.scfLoops[i]
    for (const atom of loopData.structure.atoms) delete atom.radius
    expect(loopData.finalScfEnergies).toEqual(scfLoopsTestOutput[i].finalScfEnergies)
    expect(loopData.forces).toEqual(scfLoopsTestOutput[i].forces)
    expect(loopData.maxForceComponent).toEqual(scfLoopsTestOutput[i].maxForceComponent)
    expect(loopData.structure).toEqual(scfLoopsTestOutput[i].structure)
    expect(loopData.iterations).toEqual(scfLoopsTestOutput[i].iterations)
    expect(loopData.isConverged).toEqual(scfLoopsTestOutput[i].isConverged)
  }
}
