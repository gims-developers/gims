/**
 * @author Andrey Sobolev
 *
 * @fileOverview Tests for output analyzer utilities
 */

import {deepMerge} from "../../output-analyzer-mod/util";


test('Update objects', () => {
	let target = {
    a: 1,
    b: {x: 2, y: 3},
    c: true
  }
  let update = {
    b: {x: 5},
    c: false
  }
  expect(deepMerge(target, update) === {
    a: 1,
    b: {x: 5, y: 3},
    c: false
  })
})