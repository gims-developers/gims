#!/usr/bin/env bash

# This file tries to run pyinstaller to bundle gims into a single executable

pyinstaller \
   --onefile \
   --exclude-module=tkinter \
   --exclude-module=matplotlib \
   --hidden-import=gemmi \
   --hidden-import=spglib \
   --hidden-import=spglib.spglib \
   --add-data "../app/gims/static:static" \
   -n GIMS \
   ../app/gims/__init__.py
