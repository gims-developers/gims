import sys
import fileinput
import re


def check_new_version(new_version):
    try:
        sem_vers = new_version.split('.')
        assert len(sem_vers) == 3
        for tag in sem_vers:
            assert isinstance(int(tag), int)
    except:
        raise


new_version = sys.argv[1]
print("Update all version tags to: ", new_version)

package_json = "../client/package.json"
setup_py = "../app/setup.py"
index_html = "../client/index.html"
where_to_find = "../docs/whereToFind.rst"

check_new_version(new_version)

found = False

for line in fileinput.input(package_json, inplace=True):
    if '  "version":' in line:
        found = True
        print(f'  "version": "{new_version}",')
    else:
        print(line, end='')

if not found:
    print("Did not find a version tag, where it should be:",package_json)

found = False

for line in fileinput.input(setup_py, inplace=True):
    if '    version="' in line:
        found = True
        print(f'    version="{new_version}",')
    else:
        print(line, end='')

if not found:
    print("Did not find a version tag, where it should be:",setup_py)

found = False

for line in fileinput.input(index_html, inplace=True):
    if ">Version" in line:
        found = True
        print(re.sub(r"[0-9]+\.[0-9]+\.[0-9]+",new_version,line), end='')
    else:
        print(line, end='')

if not found:
    print("Did not find a version tag, where it should be:",index_html)

found = False

for line in fileinput.input(where_to_find, inplace=True):
    if "Currently, this server runs version ``" in line:
        found = True
        print(re.sub(r"[0-9]+\.[0-9]+\.[0-9]+",new_version,line), end='')
    else:
        print(line, end='')

if not found:
    print("Did not find a version tag, where it should be:",index_html)


print('Done')
