.. GIMS documentation master file, created by
   sphinx-quickstart on Tue Feb  4 18:05:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Graphical Interface for Materials Simulations
=============================================

GIMS (Graphical Interface for Materials Simulations) is a browser-based toolbox for electronic structure codes and supports the generation of input files for first-principles electronic structure calculations and workflows, as well as the analysis and visualization of the resulting data extracted from the output files.

.. figure:: images/docsOverview.png
   :align: center
   :width: 98%


.. toctree::
    about
    whereToFind
    userManual/userManual
    developerManual/developerManual
    releaseNotes/releaseNotes

..
  Indices and tables
  ==================

  * :ref:`genindex`
  * :ref:`modindex`
  * :ref:`search`
