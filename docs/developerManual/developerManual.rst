
Developer Manual
================


This part of the documentation is intended to give an overview for developers of GIMS.

.. toctree::

    installation
    codeStructure
    contributing
    deployServer
