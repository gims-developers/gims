

Deploy the app on a server
==========================

We are assuming that you have a ``ubuntu`` server running with ``apache2``. If you need some help with that we recommend the official documentation for ubuntu here:
`<https://ubuntu.com/tutorials/install-and-configure-apache>`_

The proper configuration of the apache web server might quite specific to your local IT infrastructure, so we cannot describe it here in detail.

Before actually starting you have to install the following in addtion:
::

    sudo apt-get install libapache2-mod-wsgi-py3

Please create folder in ``/var/www/`` named ``gims_gui`` and clone or copy the source code into that folder. This will result in the following directory tree:
::

    /var/www/gims_gui
      |-- gims.wsgi
      |-- gims
            |-- app
                  |-- main.py
                  |-- static

The WSGI file ``gims.wsgi`` should include the following:
::

    #!/usr/bin/python3
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr)
    sys.path.insert(0,"/var/www/gims_gui/gims/app/")

    from main import app as application

Further, in ``/etc/apache2/sites-available`` you should add the following to your web site configuration file:
::

    WSGIScriptAlias / /var/www/gims_gui/gims.wsgi
    <Directory /var/www/gims_gui/gims/app/>
      Order allow,deny
      Allow from all
    </Directory>
    Alias /static /var/www/gims_gui/gims/app/static
    <Directory /var/www/gims_gui/gims/app/static/>
      Order allow,deny
      Allow from all
    </Directory>


Please make sure that you have built the actual application:
::

    cd /var/www/gims_gui/gims/client
    parcel build index.html --out-dir ../app/static --public-url ./

If you have not build the app before, go to the section :ref:`build-app`.
Finally, you have to start the service with:
::

    service apache2 start

If you just want to restart the app after an update, you can simply use:
::

    service apache2 restart

You should now see the web page running. An example for this you can find here: `<https://gims.ms1p.org>`_
