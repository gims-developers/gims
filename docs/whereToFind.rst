
Where to find GIMS
==================

Running Versions
----------------

Right now we maintain three servers running the GIMS application.

1. Stable Version
    The latest stable version runs on https://gims.ms1p.org. This server runs version ``1.1.0`` of GIMS.

2. Development Version
    The head of ``master`` branch is deployed automatically at https://gims-dev.ms1p.org.

3. Fallback Version
    The version ``1.0.9`` of GIMS (from 09.2021) can be found here: https://gims.ms1p.org/fallback/.

GitLab
------

The GIMS project is hosted on Gitlab: https://gitlab.com/gims-developers/gims.
Feel free to join the group of developers and contribute your own features to GIMS.
