
Band Structure
==============

This workflow is similar to the **Simple Calculation** workflow. The only difference is that the needed band path information is automatically generated.
Again, this workflow connects the following steps:

1. Structure Builder
2. Control Generator
3. Download Input files
4. Output Analyzer

This workflow is restricted to work only for periodic systems (for the band structure is a collection of properties of a periodic structure). So make sure you have defined lattice vectors for you structure.

The band path information is automatically generated based on the Bravais lattice from the underlying geometry. More detailed information about the detected Bravais lattice, the special points in the Brillouin Zone and their coordinates are given in the download section.
