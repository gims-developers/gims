
Simple Calculation
==================

The "Simple Calculation" is a workflow to create all needed input files for a single configuration or relaxation calculation.
It connects the following steps:

1. Structure Builder
2. Control Generator
3. Download Input files
4. Output Analyzer

For detailed information about the elemental apps, please visit the corresponding manual page.

Before starting the workflow, please choose your code (top right corner). This can't be reversed once the workflow has started.

The "simple calculation" workflow guides you through the individual steps.
You can navigate forward and backward using the button **Go ahead** and **Go back**, respectively. 
When you reach the download page (*Download the input files and start your calculation*), you can download the prepared input files as a ``input_files.tar`` file.
The file will be saved in your download folder.
(Please note: due to browser security restriction, we cannot let you choose the folder. However, you can change the download folder in the settings of your browser. We recommend to just move the tar file to the right place, manually.)
The tar file contains all needed input files to start the calculation.
Note: GIMS is not capable of running the actual calculation, nor start or submit such a calculation. You have to copy the input files to the computing resources of your choice and run the calculation there.
