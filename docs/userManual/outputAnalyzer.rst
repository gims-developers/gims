
Output Analyzer
===============

The *output analyzer* app visualizes output files of implemented electronic structure codes.

Import Files
""""""""""""
Click on the button next to **Import output files** to upload your output files.
You can choose either uploading single files, batches of files or folders.
To upload folders mark the checkbox right to the upload button.

After selection the *output analyzer* figures out the used code and parses the files.
The result of the parsing is shown and organized in different sections.

System Information
""""""""""""""""""

This section gives details about the investigated system, that is, chemical formula and number of atoms. Right to this section the system is visualized. In case of a structural relaxation you can watch an animation, one picture per optimization step. To start the animation, click on the **play** symbol. The optimization progress is given left to the play button, where 0 refers to the input structure.

Results
"""""""

If the calculation has finished as expected, this section summarizes the most important characteristics, e.g., total energy, or information about electron levels.

If the calculation included a relaxation of the structure, the final geometry can be downloaded by clicking on "Download final geometry" in the last line of the Results section.

Calculation Summary
"""""""""""""""""""

Summarizes information about the code itself (e.g. code version, commit number), used computational resources and runtime information (e.g. number of tasks, memory calculation time).

The exit status "Calculation exited regularly" indicates, whether the parsed output file ended correctly and as expected. That means, that the calculation converged to a results, ended within the wall time, and did not crash. If the exit status "Calculation exited regularly" returns **no**, you should manually inspect the output file. Further, for the exit status **no**, no result should be shown, except for the following case: calculations that contain more than one SCF cycle, e.g. optimization of structures. For such a case, the results for the last converged SCF cycle is shown.

Within the results section convergence graphs appear. In case of a relaxation, properties per SCF cycle are visualized, e.g. the difference in total energy or the maximum force component. Moreover, convergence of quantities per SCF iteration of a single SCF cycle are shown as well (the only one in case of a single point calculation).

DOS and Band Structure Graphs, Brillouin Zone Viewer
""""""""""""""""""""""""""""""""""""""""""""""""""""

If the corresponding output files of a Density of states (DOS) or band structure calculation are uploaded, the GIMS output analyzer automatically generates corresponding graphs from this output files. Each graph can be edited by the control elements to the right of the graphs. Editing of the graphs includes:

* Zooming in and out of an energy window
* Changing the colors and thickness of the DOS and band structure lines
* Adapting the axis labels font size and color
* Changing the labels name of the axes

In case of a periodic structure (structure has three lattice vectors) the corresponding Brillouin zone (BZ) and the band path as written in the uploaded in- or/and output files are shown.


Input files
"""""""""""

The code parser also provide the input file for download, in case they have been provided as upload or can be generated from the output file (some codes write the input files in the output file, so we use this to generated them again).

Parsing Errors
""""""""""""""

This section (hopefully) appears, if something went wrong while parsing the output files.
