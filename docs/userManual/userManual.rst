================
GIMS User Manual
================

This part of the documentation is concerned with the description of the app functionalities. Navigate through the table of contents to learn details about the GIMS application.

.. toctree::

    prerequisites
    mainPage
    structureBuilder
    controlGenerator
    outputAnalyzer
    simpleCalculation
    bandStructure
    GWCalculation
    MDCalculation
