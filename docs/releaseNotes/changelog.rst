.. _changelog:

Release 1.2.0
--------------

.. current_release_start

Version ``1.2.0``, released in the end of September, 2023, adds several more new features to `GIMS <https://gims.ms1p.org/>`_:

- **Structure Builder**
    - Optimized the *Structure Viewer* code, making it possible to visualize structures consisting of more than 15,000 atoms.
- **Control generator**
    - Added sanity checks, which are used to compare the chosen calculation inputs against the structure and throw an error (or warning) if inconsistency is found.
    - Added new *Extra keywords* table, that allows a user to make as complicated `control.in` file as they need (used at the user's own risk; no constraints or sanity checks are run).
    - Rewritten and clarified help tooltips for the majority of the input parameters.
- **Output analyzer**
    - Completely refactored Output Analyzer code.
    - Made `plotly <https://plot.ly/>`_ the new plotting backend.
- **Other**
    - Added several new calculation apps, for GW and MD calculation, with included help and sanity checks.
    - Added CI pipeline to automatically produce standalone applications for Linux and Windows.
    - Added the first version of *GW workflow*, making it possible to produce the inputs and compare the outputs for several calculations at once.
    - Tests added, bugs fixed.

Should you come across a bug, please don't hesitate to report it to `our issue tracker <https://gitlab.com/gims-developers/gims/-/issues>`_.


.. current_release_end

Release 1.1.0
--------------

    *Well, it seems that some undocumented work has been done over the years...*

Version ``1.1.0``, released in February, 2023, adds the following functionality to `GIMS <https://gims.ms1p.org/>`_:

- **Structure Builder**
    - A new *Surface (Slab) construction* field: allows a user to build the slab from the crystalline structure
      based on the Miller indices of the slab surface, the desired number of atomic layers of the slab and the
      thickness of the vacuum layer surrounding the slab. Once the slab is built, one can also terminate it with a layer
      of the desired chemical composition.
    - Added the `Constrain/Release all atoms` button to the `Atoms` section, making it simple to add a constraint to all
      atoms in one mouse click.
- **Control generator**
    - Completely refactored Control Generator Form creation, effectively decoupling from the components the logic of one Form component
      acting on another. This made possible advanced filtering of the components shown based on
      the state of other components and the Structure given in the previous step.
    - Introduced a new Form component, ``RadioField``, used for exclusive selection of one component from the group.
    - Added the output flags for Mulliken and Hirshfeld charge distributions.
- **Output analyzer**
    - Added Mulliken and Hirshfeld charge distribution visualization.
- **Other**
    - Fixed a bug with symmetry threshold not propagating correctly across different parts of code.
    - Added GIMS version selector in the header.
    - Rewritten the Docker file to get more stable and production-ready Docker environment; added CD rule pointing
      `GIMS-dev server <https://gims-dev.ms1p.org/>`_ to the HEAD of the ``master`` branch of the repository.
    - Added end-to-end testing.

  Many bugs are fixed, (I hope not so) many are introduced. If you encounter a bug, we'd be happy if you report it
  to `our issue tracker <https://gitlab.com/gims-developers/gims/-/issues>`_.

Release 1.0.2
-------------

Official Release for the JOSS Paper! Thanks for all who contributed!
A special thanks to JOSS and the reviewer for the immense helpful comments!

* Restructuring of the backend 
* Improvement of the documentation
* Improvement of the paper
* Adding a docker file
* Adding contribution guidelines

And a few small other things.


Release 1.0.0
-------------

*(From June 03, 2020, to October 06, 2020)*

With this new release we introduce semantic versioning of GIMS. The versioning we had before is deprecated. Once this release is finished, the corresponding commit will be tagged

Changes:

* **Structure Builder**
    * Multiple structure file upload: user can select several structure files. All uploaded structures are accessible via tabs.
    * New Supercell Button: user can create supercell either with simple multiples of lattice vectors (3 parameters) or with a supercell matrix (9 parameters). The new supercell will be created in a new tab.
    * Structure Info field: user gets additional information about structure, e.g. chemical formula, space group, occupied Wyckoff positions, ect. This feature integrate spglib in the backend of GIMS.
    * Primitive Button: New button in the side panel. It creates a new primitive structure (periodic systems only) in a new tab.
    * Brillouin Zone Viewer: If a periodic structure is loaded, the GIMS structure builder will automatically view the Brillouin zone of the Bravais lattice of the loaded structure. The BZ path according to Setyawan/Curtarolo, the cartesian Axis, and the reciprocal unit cell are shown as well.

* **Control Generator**
    * Units (if applicable) are added to the input values
    * Rework input value structure (code structure): Each code has its own list of supported input values. Makes it more easier to include new codes.

* **Output Analyzer**
    * Brillouin Zone Viewer added. BZ viewer displays actual path used in the calculation.

* **GIMS Backend**
    * Restructuring of the backend: Backend is now organized as a python package (which simply allows a ``pip install .`` of the backend).
    * Extending backend tests.

* **GIMS Frontend**
    * Restructuring of the dependencies: Some libraries, where the source code has been included in GIMS, have been removed. Instead, the corresponding libraries have been added as a dependency to the ``package.json`` file.

* **GIMS landing page**
    * Small redesign to adapt the content to the description in the paper.
    * Add linkt to running GIMS versions and to the release notes.


Seventh release (*r7*)
----------------------

*(From March 18, 2020, to April 7, 2020)*

This release improves several aspects of the project like testing, code documentation and adds some enhancements:

- Enhancement: **Consistent browser and in-app navigation**. The usage of the browser native navigation elements is now consistent with the in-app navigation elements for the stand-alone apps as well as for the workflows.

Some important changes were required in the application:

- Support of **multiple instances of modules and nested modules** inside another (e.g. `StructureBuilder` inside a workflow module). The `StructureBuilder` state is not a singleton longer.
- Complete **rewriting of the workflow implementation**: The workflows are now implemented like application modules (`Workflow.js`) and contain UI components (modules or regular/simpler component)
- URL based **navigation point for every step in workflows**: The URL is this way: `#WorkflowName-workflow#StepComponentId`
- Wiki **documentation** piece updated and extended: `Implementation details: workflows, application navigation <https://gitlab.com/gims-developers/gims/-/wikis/Implementation-details:-workflows,-application-navigation>`_. And `Gitlab issue explaining the development process <https://gitlab.com/gims-developers/gims/-/issues/29>`_
- **Extension of the Front-End CI Testing-Suite**. Some parts of the client are tested:
    - ``Output Analyzer`` module testing
    - The **main output file parsing** is tested for **many combination of calculations** (Single-point calculation, relaxation / Spin none, collinear / Band structure+DOS / Periodic, non-periodic / Exciting, FHI-aims).
    - Files parsing. The parsing of **all the files making up the output** is tested.
    - The parsing of the **files containing the DOS and BandStructure data** is tested.
- **File formats generated for exporting** in `Structure Builder`. The functions that generate the different combination of file formats (molecule/periodic, aims/exciting, fractional/cartesian coordinates) to be exported are tested.
- `More details in this Gitlab issue <https://gitlab.com/gims-developers/gims/-/issues/32>`_

- **Code Documentation of Front-End**. All the application code has been documented according to the **JSDoc** style

- Enhancement: **Refactoring Output-Analyzer**. The *OutputAnalyzer* module has been deeply refactored, both the *FHIaims* and *Exciting* code parsers (Sebastian) and the user interface components: The `OutputAnalyzerMod.js` has been refactored to (electronic-structure-)code-independent.

- Some minor UI improvements

Sixth release (*r6*)
--------------------

*(From November 18, 2019, to February 4, 2020)*

The main feature in this release is the addition of *Exciting* code support. On the other hand, we've added many interactivity possibilities on graphs, mainly on the *Band Structure* and *DOS* plots. As this was such important release we took the opportunity, as well, to carry out several meaningful improvements (regarding design and UX) along the application:

Main r6 features:

- **Exciting code support** (major application refactoring and new functionality):
    - Code selector IU component and corresponding new application state and logic
    - Update of the import/export functionality (*StructureBuilder* module)
    - Control generator module adaptation: new form fields, server side development (Sebastian) and integration with client.
    - Workflow adaptation (code election disabled inside, workflow configuration improvement, etc)
- **Advanced interactivity in graphs**:
    - Structure Builder: possibility of color change of the showing species (by clicking at the species circles on the legend)
    - Band Structure and DOS plots:
        - Possibility of taking screenshots of the plots
        - The plot labels are now editable and adjustable in size and color and axes ticks labels in size
        - The lines (by spin) are adjustable in color and thickness
        - Button that returns to the original state of the plot
        - Zooming improvements
- Final release improvements:
    - Explanatory text for the top level page buttons
    - **Back/Forward browser buttons support** (consistent application behavior)
    - **Application Settings**: UI and event system to change characteristics along the application. First case: *Number of decimal digits*
    - **Tooltip system for user help**. First application: field labels in *ControlGenerator*
    - User Feedback button pointing to the issue tracker on *Gitlab*
    - **Application breadcrumbs**: navigation system that shows the user where (module) is in the application
    - Several minor design and interaction improvements along the application

More work done this time:

- New own **2D graphic library** (`Canvas.js` file)
- *InfoCanvas* and DOS/BS plots refactoring (on the new library) and performance optimization.
- Color picker integration (first use: species color change - Structure builder)
- Application refactoring (images imports) and application resources review and cleaning
- *Control Generator* improvement for the *Band Structure* workflow
- Client adaptation to support `tar` file. Input files requested are now bundled (`tar` format) as the response
- Beginning of the formal code documentation

Fifth release (*r5*)
--------------------

*(From September 19 to November 15, 2019)*

The main feature in this release is the addition of a Band Structure and DOS workflow and their visualization possibility on an improved *Output Analyzer*. On the other hand, we have found, tested and implemented a way to bundle and distribute the application as a desktop/offline app.

Main r5 features:

- **Band Structure and DOS data visualization** integrated in the *Output Analyzer*:
    - *Output Analyzer* file import logic and multi-file support (folder source support).
    - Bands and DOS files data parsing and flexible graphical representation
- New **Band Structure & DOS workflow** and improved *Control Generator*:
    - New *Band Structure & DOS workflow*
    - Improved *Control Generator*: More flexible and supporting sections (Accordion user interface)
    - Backend based *control.in* generation (use the *control.in* writer from ASE). (by Sebastian)
- Application **packaging and distribution as a desktop/offline** app research and first implementation:
    - Research into the *PyInstaller* tool and decision of adoption
    - Testing on Linux and MacOS, optimization and documentation
    - The desktop application (versions for Linux and MacOS) is already downloadable (via the web application)
- Top level/dashboard restructuring:
    - Change of workflows, layout and buttons

More work done this time:

- **Error handling** through the application. Refactoring and documentation ([Application error handling](Application-error-handling) doc on the Wiki)
- Structure builder improvements (by Sebastian)
    - Simple supercell handling at the right panel of the structure builder UI
    - Constrain atoms via a checkbox at the basis atoms panel. The material of the corresponding atom changes.
    - The distance between atoms is checked. If they become too close, then, the bond color changes to red and the bond radius is increased.
- User feedback UI component at application level. A way to give simple feedback to the user (errors, warnings and informative tips). It's a text box at the top-center of application layout
- Meaningful modification of FileImporter component
- Simpler console logging/error call (Global utility functions)


Fourth release (*r4*)
---------------------

*(From August 5 to September 18, 2019)*

This time we kept expanding the application: a new module *Control generator* has been added, as well as a new important application feature: workflows support.

Main r4 features:

- ***Control Generator* application module**:
    - It generates *control.in* files from a user form and species defaults files
    - New generic ***Form*** component
    - URL fragment associated to the module (*#ControlGenerator*)
- **Workflows** support:
    - UI header (*AssistantHeader*) enables the workflow interaction
    - Declarative (partially) workflow definition
    - Two workflows (similar, only declarative difference) implemented
    - Modules adaptations to be integrated in the workflows

More work done this time:

- ***ASE* library integration** and server request based geometry importer (*Structure Builder* module)
- Import **geometry file format resolution** user interface
- Some work on the adaptation and integration of the *DOS* and *Band Structure* graphs (from the *NOMAD* project).
- Some refactoring (asynchronous programming based since now on *promises* and *async/await*) and documentation (Workflows feature)


Third release (*r3*)
--------------------

*(From May 2 to July 26, 2019)*

From this version, the application is more than a *Structure builder*. This becomes a module in the app and a new one is added: the *Output Analyzer*.

- Improvements on the previous (r2b) release:
    - Supercell construction based on a supercell matrix (based on Sebastian’s code)
    - Refactoring: ES6 import/export and dependency reduction on the math.js library (goal: get rid of it)
    - New Switch UI component (used for Atom displacement)
    - Add a checkbox to the lattice vector section to enable/disable the scaling of atom positions as lattice vectors change

Main r3 features:

- **Non-periodic systems** support (Structure Builder module): file parsing, Structure viewer and UI adaptation
    - Lattice vectors can be removed or created at any moment and circumstance
    - The lattice vectors creation/removals are supported by the undo/redo system
- Top level **application module: Dashboard**.
- Application **Output Analyzer module**:
    - Output file(s) importer: simple and relaxation calculation output support (integration of Sebastian’s file parsing code)
    - Module page layout (divided in sections)
    - Integration of the StructureViewer and development of structure animations support
    - 2D charts integration and enhancement from Sebastian’s code
    - Input files: geometry.in and control.in popup viewer and downloader
    - URL fragment associated to the module (#OutputAnalyzer)

More work done this time:

- Refactor the **StructureViewer as an app library** (reusable application module)
- Create a changelog document (Gitlab wiki)
- Research and new doc (Gitlab wiki): **Web client architecture and technical decisions**
- Modal popup component
- Some research and discussion about the next big goal: the first complete workflow


Second release - second part (*r2b*)
------------------------------------

*(From March 5 to April 29, 2019)*

- Improvements on the previous release:
    - In an atom displacement, new bonds are calculated and automatically updated on the viewer and the new atom coordinates are shown (atom info area) at mouse release time
    - Fixed performance penalty issue (mathjs library methods) related to bonds calculation
    - Screenshot feature improvement: take a screenshot in one click
    - Unify and make well-proportioned the arrows of the lattice vectors and labels for any cell size.
    - The corresponding atom is highlighted when the mouse pointer hovers the atom row (right panel)

Main features:

- **Measurements** of distance between atoms (2 atoms selected), angle (3 atoms selected) and torsion angle (4 atoms selected). The info is shown on the top-right part of the viewer.

- Possibility of creating **supercells** from the current cell. URL fragments interface: this funtion is launched by adding a fragment to the main URL. e.g. `URL#repeat:2:2:3`

- **Undo/redo feature**: system for going back and forth in the history of changes (both via UI and key combination Ctrl+z Ctrl+y). Changes supported: atom creation, move, species change, removal, lattice vectors modification and supercell transformation.

More work done:

- Important refactoring. The `Reactjs` library was removed from the project and implemented a new client architecture.
- **Transfer the application to the FHI infrastructure**
- **Performance and memory optimization** research, testing and implementation of measures
- We started the **project documentation** on gitlab (wiki section) with three pages: Development roadmap, Performance optimization and Setup WSGI for gui on apache2. We intent to document every meaningful aspect of the project from now
- **Atom selection mechanism** (multi-selection enabled). Now there are 3 atom interaction ways: hovering, selection and drag&drop. Selection: Shift key + mouse click. Clear selection: mouse right-click
- Formalization of the URL fragment (browser feature) use (starting with the hash symbol - #). For now only param repeat supported


Second release - first part (*r2a*)
-----------------------------------

*(From early January to March 1, 2019)*

- Improvements of the previous release:
    - Bonds representation improvement (algorithm to identify bonds across cell boundaries)
    - UI element to switch between fractional and cartesian coordinates
    - More digits (5) in the coordinate fields. Several solutions were considered and visually tested
    - More options (left-top checkboxes) added to the 3D viewer:
        - Option of switching on/off the lattice vectors
        - Option of switching on/off the unit cell box
        - Wrap atoms into cell (If atoms are outside the box, move them to the equivalent positions inside the box)
        - Option of switching on/off the bonds across the cell boundaries

Main features:

- Structure **creation from scratch**
- **Manipulation of imported** (from files) structures. You can (editing text fields):
    - edit the lattice vectors (modify the cell) and the atoms coordinates (move the atoms)
    - change the species (substitution of atoms)
    - add and remove atoms
- **Enabled interaction** with the structure elements **on the 3D viewer**. You can:
    - Move atoms dragging them (mouse click and release)
    - Show textual info (on the viewer) of the atom hovered by the mouse pointer
    - Atom highlights (in 3D view) by hovering the circle in the atom row (side panel)
- **Export** of the editor current state of the structure in 'geometry.in' format (both in fractional and cartesian coordinates)

More work included in this release:

- **Screenshot capture** functionality

- Moving atoms with the mouse enabled via a checkbox (to avoid accidents)

- Side panel **new button bar** (*Import - Export - Reset*) and user interaction

- Default structure when the web app is loaded

First release (r1)
------------------

*(From mid November to the end of December, 2018)*

Main features:

- **Work environment set-up**: `Reactjs` (frontend library) + `Flask` (python backend) + `GoogleAppEngine` (cloud hosting)

- **Structure 3D viewer**: based on `Threejs` library (using *WebGL* browser capability)
  - Basic cell, atoms and lattice vectors and parameters representation
  - Interaction: zoom and rotation
  - Bonds and atoms on cell boundaries
  - Legend and structure visualization control

- **Side panel** showing lattice vectors and atoms alphanumeric data

- **Import of existing structures** feature. Formats supported:
  - FHI-aims format ‘geometry.in’
  - Crystallographic Information File (CIF)
