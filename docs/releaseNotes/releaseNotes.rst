Release Notes
===============

The following threads will keep you updated on the upcoming GIMS releases.

Current Release
---------------

.. include::  changelog.rst
    :start-after: .. current_release_start
    :end-before: .. current_release_end

Full changelog
--------------

The section *Changelog* describes and documents the implementation of new features, fixes and so on, of all releases.

.. toctree::

    changelog
