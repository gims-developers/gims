"""Some tests to check CIF file preprocessor"""
import tempfile
from pathlib import Path

import pytest

from gims.cif import CifPreprocessor

TEST_DIR = Path(__file__).parent


def test_cif_preprocessor_read():
    """Tests reading and parsing CIF info from various sources"""
    file_path = TEST_DIR / 'Structures' / 'Si.cif'
    # path
    cif = CifPreprocessor(file_path)
    cif._parse()
    assert cif.space_group == 1

    # file object
    with open(file_path) as cif_file:
        cif = CifPreprocessor(cif_file)
        cif._parse()
        assert cif.space_group == 1

    # file object with binary access
    with open(file_path, 'rb') as cif_file:
        cif = CifPreprocessor(cif_file)
        cif._parse()
        assert cif.space_group == 1

    # two blocks
    with open(file_path) as f:
        cif_lines = f.readlines()
    with tempfile.TemporaryDirectory() as temp_dir:
        temp_file = Path(temp_dir) / 'Si.cif'
        temp_file.touch()
        with pytest.raises(ValueError, match='Did not find block with data'):
            _ = CifPreprocessor(temp_file)

        with open(temp_file, 'w') as f:
            two_blocks = (cif_lines + ['data_Si_standardized_unitcell\n'] + cif_lines[1:] +
                          ['Si3      1.0    0.5000      0.5000      0.5000     Biso  1.000  Si\n'])
            f.write(''.join(two_blocks))
        cif = CifPreprocessor(temp_file)
        cif._parse()
        assert len(cif.coords) == 3


def test_cif_preprocessor_save():
    """Tests saving preprocessed info to CIF"""
    file_path = TEST_DIR / 'Structures' / 'Si.cif'
    cif = CifPreprocessor(file_path)
    with tempfile.TemporaryDirectory() as temp_dir:
        temp_file = Path(temp_dir) / 'Si.cif'
        cif.save(temp_file)
        cif = CifPreprocessor(temp_file)
        cif._parse()
        assert cif.space_group == 1




