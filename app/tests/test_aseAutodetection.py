from ase.io import read, write
from pathlib import Path

path_prefix = Path('tests/Structures')
ref_structures = {
    'cif': {
        'path': path_prefix / 'Si.cif',
        'symbols': 'Si2'
    },
    'aims':{
        'path': path_prefix / 'geometry.in',
        'symbols': 'CdSrTaCdSrTaCdSrTaCdSrTa'
    },
    'exciting':{
        'path': path_prefix / 'input.xml',
        'symbols': 'Hf2'
    },
}

class TestAutodetection:

    def test_all(self):
        for key, value in ref_structures.items():
            c = read(value['path'])
            assert str(c.symbols) == value['symbols']
