from gims.prepare_input import structure_auto_detection
from gims.structure import read


class TestStructureIO:

    def test_write_json_from_atoms(self):
        # with client.session_transaction() as sess:
        #     sess['symThresh'] = '0.00001'
        file_name = 'tests/FHIaims/test_files/geometry.in'
        with open('tests/FHIaims/test_files/structure.json') as json_file:
            json_ref = json_file.read()
        a = read(file_name)
        json_out = a.to_json(file_name, sym_thresh=1e-5)
        assert json_out == json_ref

    def test_structure_auto_detection_success(self):
        file_name = 'tests/FHIaims/test_files/geometry.in'
        json_out = structure_auto_detection(file_name, {"symThresh": 1e-05})
        with open('tests/FHIaims/test_files/structure.json') as json_file:
            json_ref = json_file.read()
        # for key, value in json_ref[:-1]
        print(json_out)
        print(json_ref)
        assert json_out == json_ref

    def test_structure_auto_detection_fail(self):

        file_name = 'tests/Exciting/test_files/input.fail'
        json_out = structure_auto_detection(file_name, {"symThresh": 1e-05})

        assert "ErrorParsingGeometryFile" in json_out
