"""A Structure class based on ase.Atoms"""

import json
import os
import numpy as np

from ase import Atoms
from ase.constraints import FixAtoms
from ase.io import read as ase_read

from gims.structure_info import StructureInfo


class Structure(Atoms):

    @classmethod
    def from_dict(cls, struct_dict):
        """Generates an ASE atoms object from the structure attached to the
        control-generator form.

        Parameters:
            struct_dict (dict):
                Dictionary containing the structure, which was sent from the client.
        """

        cell, pbc, positions, species, constraints, mag_moms, charges, c = (None,) * 8
        # find the keys in the dicts
        cell_name = 'cell' if 'cell' in struct_dict else 'latVectors'
        atoms_name = 'atoms' if 'atoms' in struct_dict else 'positions'

        if cell_name in struct_dict:
            cell = struct_dict[cell_name]
            pbc = True

        if atoms_name in struct_dict:
            positions = [p["position"] for p in struct_dict[atoms_name]]
            species = [p["species"] for p in struct_dict[atoms_name]]
            mag_moms = [p["initMoment"] for p in struct_dict[atoms_name]]
            constraints = [p["constraint"] for p in struct_dict[atoms_name]]
            charges = [p["charge"] for p in struct_dict[atoms_name]]

        if any(constraints):
            c = FixAtoms(mask=constraints)
        # print(positions,cell,species)

        return cls(
            symbols=species,
            positions=positions,
            magmoms=mag_moms,
            charges=charges,
            constraint=c,
            cell=cell,
            pbc=pbc,
        )

    @classmethod
    def from_form(cls, form_dict, is_periodic):
        """Generates a dummy ASE atoms object only from the control-generator form,
        if no structure was attached to it.

        Parameters:
            form_dict (dict):
                JSON Form data from the control generator. A list of species from the Form
                is needed.
            is_periodic (boolean):
                Whether the pseudo structure should be periodic or not.
        """
        cell, pbc = None, None
        if is_periodic:
            # Need dummy cell
            cell = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
            pbc = True
        return cls(symbols=form_dict["species"], cell=cell, pbc=pbc)

    @classmethod
    def from_atoms(cls, atoms):
        """Generates Structure instance from the parent Atoms instance

        Parameters:
            atoms (Atoms):
                an ase.Atoms instance
        """
        instance = cls()
        instance.__dict__ = atoms.__dict__
        return instance

    def get_info(self, sym_thresh):
        """Get additional supportive information about structure.

        Parameters:
            sym_thresh (float):
                Symmetry threshold for determining symmetry information for periodic
                structures.
        """
        s_info = StructureInfo(self, sym_thresh)
        return s_info.get_info()

    def get_primitive_cell(self, sym_thresh):
        """Get primitive cell

        Parameters:
            sym_thresh (float):
                Symmetry threshold for determining symmetry information for periodic
                structures.
        """
        s_info = StructureInfo(self, sym_thresh)
        return Structure.from_atoms(s_info.primitive)

    def get_conventional_cell(self, sym_thresh):
        """Get conventional cell

        Parameters:
            sym_thresh (float):
                Symmetry threshold
        """
        s_info = StructureInfo(self, sym_thresh)
        return Structure.from_atoms(s_info.conventional)

    def to_json(self, file_name, sym_thresh):
        """Converts ASE atoms object 'struct' to json object.

        Parameters:
            file_name (str):
                Name of the structure (inherited from the original file name)
            sym_thresh (float):
                Symmetry threshold for determining symmetry information for periodic
                structures.
        """

        json_s = {"lattice": [],
                  "atoms": [],
                  "fileName": os.path.basename(file_name),
                  "structureInfo": self.get_info(sym_thresh)}

        # print(json_s["structureInfo"])
        pbc = np.any(self.pbc)
        if pbc:
            for v in self.cell.array:
                json_s["lattice"].append(list(v))
        else:
            json_s["lattice"] = None
        atoms_data = zip(
            self.get_positions(),
            self.get_chemical_symbols(),
            self.get_initial_magnetic_moments(),
            self.get_initial_charges(),
        )
        # print(struct.constraints[0].index)
        for i, (pos, species, init_moment, charge) in enumerate(atoms_data):
            # Tried to bring it in the correct form for this function:
            # structure.addAtomData(atomPos, tokens[4], tokens[0] === ATOM_FRAC_KEYWORD)
            # in src/common/util.js. The last entry is always false, since we obtain
            # cartesian positions from ase.
            constraint = False
            try:
                if i in self.constraints[0].index:
                    constraint = True
                    # print([list(pos), species, "", constraint])
            except IndexError:
                pass
            json_s["atoms"].append([list(pos), species, "", init_moment, constraint, charge])
        return json.dumps(json_s)


def read(file_name, *args, **kwargs):
    """A revamped implementation of ase.io.read that returns gims.Structure"""
    return Structure.from_atoms(ase_read(file_name, *args, **kwargs))
