""" A module for slab building
"""
import json
from typing import Iterable, List, Tuple, Union, Optional
import numpy as np
from ase.build import sort, surface
from ase.formula import Formula
from gims import constants
from .structure import Structure


class Slab:
    """ A class representing the slab itself. If `get_slab` is not called, then the
    Atoms object passed to the constructor is a slab
    """

    def __init__(self, atoms: Structure, tol: float = 0.002) -> None:
        self.atoms = atoms
        self.tol = tol
        self.z_shifts = None
        self.extras = {}
        self.is_slab = False
        self.is_terminated = False

    @classmethod
    def from_json(cls, data_json: Union[str, bytes]) -> 'Slab':
        """Generates a Slab instance from a given JSON
        """
        data = json.loads(data_json)
        try:
            atoms = Structure.from_dict(data)
        except ValueError as ex:
            raise ValueError('Something went wrong with Atoms object generation') from ex
        instance = Slab(atoms)
        instance.extras = {k: v for k, v in data.items() if k not in ['cell', 'positions']}
        return instance

    def get_slab(self, indices: Optional[Iterable[int]] = None,
                 layers: Optional[int] = None,
                 vacuum: Optional[float] = None) -> Structure:
        """ Returns an ase.Atoms object of the slab asked for
        """
        if indices is None:
            indices = self.extras['slab_data']['miller']
        if layers is None:
            layers = self.extras['slab_data']['layers']
        if vacuum is None:
            vacuum = self.extras['slab_data']['vacuum'] / 2
        self.atoms = Structure.from_atoms(surface(lattice=self.atoms, indices=indices, layers=layers, periodic=True))
        self.trim(self.number_of_atomic_layers - layers)
        self.atoms.center(vacuum=vacuum, axis=2)
        self.is_slab = True
        return self.atoms

    def _get_layers(self) -> None:
        """ Gets the atomic layers (up to a tolerance).
        Tags the atoms in the slab with layer numbers
        """
        # round z coordinates to tol+2 decimal digits (hope it'll be OK)
        # due to get rid of rounding errors in ase
        zs = np.round(self.atoms.get_scaled_positions()[:, 2], int(np.log10(1 / self.tol) + 1))
        shifts, tags = np.unique(zs // self.tol, return_inverse=True)
        self.z_shifts = shifts * self.tol
        self.atoms.set_tags(tags)

    def trim(self, n: int, where: str = 'top') -> Structure:
        """Trims n atomic layers from the top or bottom of the slab
        """
        if where not in ('top', 'bottom'):
            raise ValueError('Layers can be trimmed only from the top or bottom of the slab')
        if n >= self.number_of_atomic_layers:
            raise ValueError('Can not trim more layers than are there')
        if n == 0:
            # nothing to trim here!
            return self.atoms
        if where == "top":
            last_layer = self.number_of_atomic_layers - n - 1
            idx = [atom.index for atom in self.atoms if atom.tag > last_layer]
            del self.atoms[idx]
            # change z cell coordinate, as it's always normal to layers in a slab
            self.atoms.cell[2, 2] *= 1 - (self.z_shifts[-1] - self.z_shifts[last_layer])
        else:
            idx = [atom.index for atom in self.atoms if atom.tag < n]
            del self.atoms[idx]
            crd = self.atoms.get_scaled_positions()
            crd[:, 2] -= (self.z_shifts[n] - self.z_shifts[0])
            self.atoms.set_scaled_positions(crd)
            self.atoms.cell[2, 2] *= 1 - (self.z_shifts[n] - self.z_shifts[0])
        self._get_layers()
        return self.atoms

    def terminate(self, terminations: Optional[Iterable[int]] = None) -> Structure:
        """Terminate slab with layers with given indices.

        Parameters:
        :param terminations: indices of top and bottom layers for termination. If `None`, will look into the extras
        """
        if self.z_shifts is None:
            self._get_layers()
        if terminations is None:
            try:
                terminations = [int(x) for x in self.extras['slab_data']['terminations']]
            except KeyError:
                terminations = (0, self.number_of_atomic_layers - 1)
        # first trimming from the top, then from the bottom
        bottom, top = terminations
        self.trim(self.number_of_atomic_layers - top - 1, where='top')
        self.trim(bottom, where='bottom')
        self.is_terminated = True
        return self.atoms

    def get_terminations(self, unique: bool = True) -> List[Tuple[str, int]]:
        """Returns a list of chemical formulas for slab termination layers
        """
        if self.z_shifts is None:
            self._get_layers()
        terms = []
        unique_layers = {}
        for i in range(self.number_of_atomic_layers):
            els = "".join([atom.symbol for atom in self.atoms if atom.tag == i])
            formula = Formula(els).format('hill')
            if unique:
                # try our best to find unique terminations (as of now, comparing layer formulae)
                if formula not in unique_layers:
                    unique_layers[formula] = len(unique_layers)
                terms.append((formula, unique_layers[formula]))
            else:
                # we treat every layer as unique
                terms.append((formula, i))
        return terms

    def _prettify_atoms(self) -> None:
        """Sorts atoms according to their z-positions and wraps their coordinates to cell, 
        effectively minimizing them
        """
        self.atoms.wrap(pbc=True)
        self.atoms = sort(self.atoms, tags=self.atoms.get_tags())

    @property
    def number_of_atomic_layers(self) -> int:
        """Returns the number of atomic layers in a slab perpendicular to z direction.
        The atoms are said to be in the same layer if their z-coordinate is the same
        (up to a given tolerance)
        """
        if self.z_shifts is None:
            self._get_layers()
        return len(self.z_shifts)

    def get_json(self, session: dict) -> str:
        """Returns a JSON string of the slab

        Args:
            session (dict): session specific dictionary
        """
        self._prettify_atoms()
        f_name = ''
        if self.is_slab:
            f_name = f"{self.extras.get('fileName', f_name)} (slab)"
        if self.is_terminated:
            f_name = f"{self.extras.get('fileName', f_name)} (terminated)"
        sym_thresh = session.get("symThresh", constants.sym_thresh)
        data = json.loads(self.atoms.to_json(f_name, sym_thresh))
        data['terminations'] = self.get_terminations()
        return json.dumps(data)
