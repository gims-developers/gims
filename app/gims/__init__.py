import os
import sys
import json
import tarfile
import tempfile
import webbrowser
from io import BytesIO
from pathlib import Path
from threading import Timer

from flask import Flask, request, Response, redirect, url_for, session, abort, send_from_directory, Blueprint

from gims.prepare_input import generate_slab, terminate_slab


# The main.py file sets up the Flask app and handles the client requests.
# ----------------- Set up the internal paths --------------------------

if getattr(sys, "frozen", False) and hasattr(sys, '_MEIPASS'):
    # Offline version
    # noinspection PyProtectedMember
    static_folder = os.path.join(sys._MEIPASS, "static")
    # noinspection PyProtectedMember
    SPECIES_DIR = os.path.join(sys._MEIPASS, "static/data/species_defaults/")
    # print(f'I am frozen!: {static_folder}')
    app = Flask(__name__, static_folder=static_folder, static_url_path="/static")
else:
    # Server version
    SPECIES_DIR = "static/data/species_defaults/"
    app = Flask(__name__)

if os.getenv("GIMS_BUILD_SISSO_WORKFLOW", 'False').lower() in ('true', '1', 't'):
    from sisso_active_learning_workflow.app import create_sisso_activete_learning_workflow_app
    import inspect
    assets_folder = Path(inspect.getfile(create_sisso_activete_learning_workflow_app)).parent / "assets"
    @app.route('/dash/assets/<path:filename>')
    def custom_static(filename):
        return send_from_directory(str(assets_folder), filename)
    app_sisso_workflow = create_sisso_activete_learning_workflow_app(app, "/dash/")

app.secret_key = os.urandom(16)
app.config.update(
    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_SAMESITE='Lax'
)
# ----------------- Client requests start here --------------------------


@app.route("/")
def index():
    return redirect(url_for("static", filename="index.html"))


@app.route("/parse-geometry-file", methods=["GET", "POST"])
def parse_geometry_file():
    """Captures client request to parse structure data.

    Expects a text file either of unknown
    format (then the ASE auto-detection is used) or of known format (then `ase.io.read`
    for that particular format is used). If parsed successfully, returns the structure
    data in json format. Otherwise, returns error message.
    """
    from gims.prepare_input import get_json_structure

    if request.method == "POST":
        json_structure = get_json_structure(request.files, session)
        return Response(json_structure, mimetype="application/json")


@app.route("/update-structure-info", methods=["GET", "POST"])
def update_structure_info():
    """Captures client request to update the supportive information (e.g. chemical
    formula, number of atoms, symmetry information for periodic structures, etc.)

    Expects structure data in json format. Returns a json structure data with updated
    supportive information.
    """
    from gims.prepare_input import update_json_structure

    if request.method == "POST":
        struct_data = json.loads(request.data)
        if "symThresh" in struct_data:
            session["symThresh"] = float(struct_data.pop("symThresh"))
        # update struct_data with unknown type
        struct_data["positions"] = [dict(**d, species="X") if "species" not in d else d
                                    for d in struct_data["positions"]]
        json_structure = update_json_structure(struct_data, session)
        return Response(json_structure, mimetype="application/json")


@app.route("/generate-control-in", methods=["GET", "POST"])
def generate_control_in():
    """Captures client request to generate input file(s) with numerical parameters and
    run-time choices (including basis set information).

    Expects a json file with the specified numerical parameters. Returns a tar-ball
    with the corresponding input file(s).
    """
    from gims.prepare_input import get_input_files
    if request.method == "POST":
        species_dir = os.path.join(app.root_path, SPECIES_DIR)
        return get_input_files(request.data, species_dir, session)


@app.route("/get-download-info", methods=["POST"])
def get_download_info():
    """Request download information before actual input files are downloaded.

    Currently, this function is used to summarize the information about the
    automatically generated band path. However, this function can be generalized
    for any future workflow.
    """
    from gims.prepare_input import get_download_info

    if request.method == "POST":
        species_dir = os.path.join(app.root_path, SPECIES_DIR)
        return get_download_info(request.data, species_dir, session)


@app.route("/get-standard-cell", methods=["POST"])
def get_standard_cell():
    """Generate standardized structure (either primitive or conventional).

    Captures client request to generate the primitive structure. Expects structure
    data in json format and returns the primitive structure, if current structure is
    not primitive. If the current structure is primitive, return message.
    """
    from gims.prepare_input import primitive_cell, conventional_cell

    struct_data = json.loads(request.data)
    if "symThresh" in struct_data:
        session["symThresh"] = float(struct_data.pop("symThresh"))
    is_primitive = struct_data.pop("is_primitive", True)
    return primitive_cell(struct_data, session) if is_primitive else conventional_cell(struct_data, session)


@app.route('/upload-workflow', methods=['POST'])
def upload_workflow_output():
    """ Unpacks and prepares the data for the post-processing on the client side

    Returns: prepared data
    """
    files = []
    f = tarfile.open(fileobj=BytesIO(request.data), mode='r:gz')
    for f_name in f.getnames():
        file_handle = f.extractfile(f_name)
        if file_handle is not None:
            try:
                files.append({"name": f_name, "content": file_handle.read().decode('utf-8')})
            except UnicodeDecodeError:
                # a binary file is found in the archive, skip for now
                pass
    return json.dumps(files)


@app.route("/get-slab", methods=["POST"])
def get_slab():
    """Generates a slab from a given data.
    """
    if request.method == "POST":
        try:
            return generate_slab(request.data, session)
        except KeyError:  # no slab_data in request
            return abort(400)


@app.route("/terminate-slab", methods=["POST"])
def term_slab():
    """Terminates a slab from a given data.
    """
    if request.method == "POST":
        return terminate_slab(request.data, session)


@app.route("/generate-workflow", methods=["POST"])
def generate_workflow():
    from gims.workflow import Workflow
    workflow = Workflow(json.loads(request.data))
    with tempfile.TemporaryDirectory() as temp_dir:
        tar_dir, tar_file, info = workflow.run(temp_dir)
        if tar_file == "FileNotFoundError":
            response = "FileNotFoundError"
        else:
            print("I am sending the tar", tar_dir, tar_file)
            response = send_from_directory(tar_dir, tar_file, as_attachment=True)
    return response


@app.route("/get-bz-vertices", methods=["POST"])
def get_bz_vertices():
    """Captures client request to calculate Brillouin Zone for a periodic structure.

    Expects the lattice vectors in json format (`[[lat_v1],[lat_v2],[lat_v3]]`).
    Returns a dictionary of vertices and band paths needed for drawing the BZ and the
    path along the high-symmetry points.
    """
    from gims.bz_utilities import calculate_bz_from_cell_json

    if request.method == "POST":
        return calculate_bz_from_cell_json(request.data)


@app.route("/get-bases", methods=["GET"])
def get_available_bases():
    """Returns a JSON with available basis directories for a given code. Should accept code name in the request"""
    code_name = request.args.get('code', None)
    if code_name is None:
        return "Code not found", 400
    species_dir = Path(__file__).parent / SPECIES_DIR / code_name
    if not species_dir.exists():
        return "Basis directory not found", 400
    return json.dumps(os.listdir(species_dir))


def open_browser():
    """Opens a browser with GIMS in it"""
    webbrowser.open_new('http://127.0.0.1:5000/')


def main():
    """Main flask application"""
    Timer(1, open_browser).start()

    app.run()


if __name__ == "__main__":
    main()
