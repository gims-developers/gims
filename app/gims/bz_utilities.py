import numpy as np
from scipy.spatial import Voronoi
import json
import itertools
from ase.cell import Cell
from ase.dft.kpoints import resolve_kpt_path_string


def calculate_bz_from_cell(cell):
    """Calculates the Brillouin Zone vertices and path of high-symmetry points.

    Parameters:

    cell: numpy array
        Three lattice vectors arranged as [[lat_v1],[lat_v2],[lat_v3]]
    """
    corners = np.array(list(itertools.product([-2, -1, 0, 1, 2], repeat=3)))
    points = np.dot(corners, cell)
    vor = Voronoi(points)
    bz_vertices = []
    for r in vor.ridge_dict:
        # This is not the general solution. It might happen that for very skewed cells
        # that next-next-nearest neighbors are building the faces of the BZ.
        if r[0] == 62 or r[1] == 62:
            bz_vertices.append([list(vor.vertices[i]) for i in vor.ridge_dict[r]])

    cell_o = Cell(2 * np.pi * Cell(cell).reciprocal())
    bp = cell_o.bandpath()
    # print(cell.get_bravais_lattice())
    r_kpts = resolve_kpt_path_string(bp.path, bp.special_points)
    segments = []
    for labels, coords in zip(*r_kpts):
        segments.append([labels, coords.tolist()])
    return {"vertices": bz_vertices, "path": segments}


def calculate_bz_from_cell_json(cell_json):
    """Convert lattice vectors (`cell`) in json format to numpy array and return
    Brillouin zone results in json format.

    Parameters:

    cell: json array
        Three lattice vectors arranged as [[lat_v1],[lat_v2],[lat_v3]]
    """
    cell = np.array(json.loads(cell_json))
    bz_vertices = calculate_bz_from_cell(cell)
    # print(bz_vertices)
    return json.dumps(bz_vertices)
